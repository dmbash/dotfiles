# $OpenBSD: dot.cshrc,v 1.10 2020/01/24 02:09:51 okan Exp $
#
# csh initialization

setenv EDITOR mg

# XDG
setenv XDG_CONFIG_HOME $HOME/xdg/config
setenv XDG_DATA_HOME $HOME/xdg/data
setenv XDG_CACHE_HOME $HOME/xdg/cache

alias df	df -k
alias du	du -k
alias f		finger
alias h		'history -r | more'
alias j		jobs -l

# LS aliases
alias ls 	ls --color
alias la	ls -a
alias lf	ls -FA
alias ll	ls -lsA

alias tset	'set noglob histchars=""; eval `\tset -s \!*`; unset noglob histchars'
alias z		suspend

set path = (~/bin /bin /sbin /usr/{bin,sbin,X11R6/bin,local/bin,local/sbin,games})

setenv GIT_BRANCH_CMD "sh -c 'git branch --no-color 2> /dev/null' | sed -e '/^[^*]/d' -e 's/* \(.*\)/ (\1)/'"

if ($?prompt) then
	# An interactive shell -- set some stuff up
	set filec
	# autocomplete
	set complete = enhance
	set autolist
	#
	set history = 1000
	set ignoreeof
	set mail = (/var/mail/$USER)
	set mch = `hostname -s`
	alias prompt 'set prompt = "%B%n@%~%b`$GIT_BRANCH_CMD`: "'
	alias cd 'cd \!*; prompt'
	alias chdir 'cd \!*; prompt'
	alias popd 'popd \!*; prompt'
	alias pushd 'pushd \!*; prompt'
	alias git 'git \!*; prompt'
	cd .
	umask 22
endif
