#! /usr/bin/sh

export EDITOR="emacs"

export GOPATH="$HOME/go"

export TERMINAL='st -e'

# XDG ENV Variables
export XDG_CONFIG_HOME="$HOME/xdg/config"
export XDG_DATA_HOME="$HOME/xdg/data"
export XDG_CACHE_HOME="$HOME/xdg/cache"

source $HOME/.bashrc
