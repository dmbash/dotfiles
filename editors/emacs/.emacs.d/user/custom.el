;; Custom file
(setq custom-file (concat user-emacs-directory ".custom.el"))
(unless (file-exists-p custom-file)
  (write-region "" nil custom-file))
(load custom-file nil t)
;; Custom file ends
