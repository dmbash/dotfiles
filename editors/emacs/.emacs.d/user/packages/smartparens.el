(use-package smartparens
  :straight t
  :after smartparens-config
  :hook (
	 (prog-mode . smartparens-mode)
	 (web-mode-hook . smartparens-mode)
	 )
  :config
  (defhydra hydra-smartparens (:hint nil :color blue)
    "
Smartparens (_q_uit)

_j_ next
_k_ previous
_x_ kill sexp
_y_ copy sexp
_s_ unwrap
_r_ rewrap
"
    ("j" sp-next-sexp :color red)
    ("k" sp-previous-sexp :color red)
    ("x" sp-kill-sexp)
    ("y" sp-copy-sexp)
    ("s" sp-splice-sexp)
    ("r" sp-rewrap-sexp)
    ("q" nil :color blue))

  (define-key prog-mode-map (kbd "C-x p") 'hydra-smartparens/body)
  )


