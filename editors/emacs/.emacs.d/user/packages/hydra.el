(use-package hydra
  :straight t
  :defer 3
  :config
  (defhydra hydra-window (:color pink :hint nil)
    "
 Movement^^ Resize^^ ^Switch^        ^Split^           ^Delete^
-------------------------------------------------------------------------------------
 [_h_] ←    [_H_] ←  [_s_]wap        [_v_]ertical      [_dw_] window
 [_j_] ↓    [_J_] ↓  [_o_]nly this   [_x_] horizontal  [_db_] buffer 
 [_k_] ↑    [_K_] ↑                                    [_df_] frame
 [_l_] →    [_L_] → 
"

    ("h" windmove-left)
    ("j" windmove-down)
    ("k" windmove-up)
    ("l" windmove-right)    
    ("H" hydra-move-splitter-left)
    ("J" hydra-move-splitter-down)
    ("K" hydra-move-splitter-up)
    ("L" hydra-move-splitter-right)
    ("v" split-window-right)
    ("x" split-window-below)
    
    ("o" delete-other-windows :exit t)
    ("s" ace-swap-window)
    ("dw" delete-window)
    ("db" kill-this-buffer)
    ("df" delete-frame :exit t)
    ("q" nil))

  (global-set-key (kbd "C-x w") 'hydra-window/body)
  )
    
