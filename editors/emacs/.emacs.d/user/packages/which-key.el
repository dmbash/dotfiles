(use-package which-key
  :straight t
  :defer 3
  :diminish which-key-mode
  :custom
  (which-key-idle-delay 0.5)
  (which-key-dont-use-unicode t)
  (which-key-setup-side-window-bottom)
  (which-key-enable-extended-define-key t)
  :config
  (which-key-mode t)
  (which-key-setup-minibuffer))
