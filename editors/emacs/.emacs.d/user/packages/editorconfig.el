(use-package editorconfig
  :straight t
  :defer 1
  :config
  (editorconfig-mode t))
