(use-package go-mode
  :straight t
  :hook (go-mode-hook)
  )

(add-hook 'go-mode-hook (lambda ()
			       (setq tab-width 4)))

