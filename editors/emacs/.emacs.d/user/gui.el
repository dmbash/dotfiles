(setq inhibit-startup-message t)

(menu-bar-mode -1)

(if (not (display-graphic-p))
    (progn
      (xterm-mouse-mode t)
      )
  )

(if (display-graphic-p)
    (progn
      (scroll-bar-mode -1)
      (tool-bar-mode -1)
      (tooltip-mode -1)

      (set-fringe-mode 10)
      )
)

(setq visible-bell t)

(when (display-graphic-p)
  (setq mouse-wheel-scroll-amount '(1 ((shift) . 1))
        mouse-wheel-progressive-speed t))
(setq scroll-step 1
      scroll-margin 0
      scroll-conservatively 101)

(setq use-dialog-box nil)

(setq-default cursor-type 'bar)

(use-package doom-modeline
  :straight t
  :defer 3
  :config (doom-modeline-mode 1)
  :custom ((doom-modeline-height 15)))

(use-package doom-themes
  :straight t
  :demand t)

(use-package paren
  :straight t
  :hook (prog-mode . show-paren-mode))

(use-package rainbow-delimiters
  :straight t
  :hook (prog-mode . rainbow-delimiters-mode))

(add-hook 'prog-mode-hook
	  (lambda ()
	    (display-line-numbers-mode t)
	    (prettify-symbols-mode t)
	    (column-number-mode t)
	    ))

;; show file size
(size-indication-mode 1)

;;; Themes
(add-to-list 'custom-theme-load-path (concat user-emacs-directory "user/themes"))
(if (not custom-enabled-themes)
    (load-theme 'zenburn t))

(use-package transparency
  :straight (transparency
	     :type git
	     :host gitlab
	     :repo "dmbash/transparency.el"
	     :branch "master")
  :defer 3)

