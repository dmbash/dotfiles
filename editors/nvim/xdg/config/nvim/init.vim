" File: init.vim
" Author: Dmitry Bashkirov <richd.pls@gmail.com>
" Description: NeoVim configuration
"
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                            Autoinstall Vim-plug                            "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
if empty(glob('$XDG_DATA_HOME/nvim/site/autoload/plug.vim'))
  silent !curl -fLo $XDG_DATA_HOME/nvim/site/autoload/plug.vim --create-dirs
    \ https://raw.githubusercontent.com/junegunn/vim-plug/master/plug.vim
  autocmd VimEnter * PlugInstall --sync | source $MYVIMRC
endif

call plug#begin(stdpath('data') . '/plugged')

  """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  "                                  Tools                                  "
  """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  Plug 'jiangmiao/auto-pairs' " Auto inserting brackets, quotes, etc
  Plug 'liuchengxu/vim-which-key' " Display keymaps
  Plug 'mhinz/vim-startify' " Startup screen
  Plug 'editorconfig/editorconfig-vim'
  Plug 'sirver/ultisnips' " Snippets engine
  Plug 'honza/vim-snippets' " Snippets for snippets engine
  Plug 'tpope/vim-surround' " Tool for managing surrounds, e.g. brackets, quotes
  Plug 'vim-airline/vim-airline' " Statusline
  Plug 'vim-airline/vim-airline-themes' " Themes for statusline
  Plug 'ctrlpvim/ctrlp.vim' " Files fuzzy-finder
  Plug 'mileszs/ack.vim' " Substring searching
  Plug 'vim-test/vim-test' " Test runner
  Plug 'neoclide/coc.nvim', {'branch': 'release'}
  Plug 'mattn/emmet-vim' 

  """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  "                             Version control                             "
  """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  Plug 'tpope/vim-fugitive'
  Plug 'airblade/vim-gitgutter'

  """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  "                                 Syntax                                  "
  """""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  Plug 'leafgarland/typescript-vim'
  Plug 'peitalin/vim-jsx-typescript'
  Plug 'posva/vim-vue'
  Plug 'pangloss/vim-javascript'
  Plug 'neovimhaskell/haskell-vim'
  Plug 'cakebaker/scss-syntax.vim'
  Plug 'maxmellon/vim-jsx-pretty'
  Plug 'Valloric/MatchTagAlways'

  """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  "                               Colorschemes                               "
  """"""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
  Plug 'morhetz/gruvbox'
  Plug 'dracula/vim', {'as': 'dracula'}

call plug#end()

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                            NeoVim configuration                            "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

let mapleader = " "
set encoding=utf8
colorscheme dracula
syntax enable


set wildignore+=*\\tmp\\*,*.swp,*.zip,*.exe
set noerrorbells visualbell
set number relativenumber
set tabstop=2 softtabstop=2 expandtab shiftwidth=2 smarttab smartindent
set incsearch
set cursorline
set list listchars=tab:>\ ,trail:-,eol:↵ " chars for trailing spaces, tabs, EOL symbols

" fix nvim-qt popup menu
if (has("gui_running"))
  au VimEnter * GuiPopupmenu 0
endif

if (has("termguicolors"))
  set termguicolors
endif

let g:netrw_banner = 0
let g:netrw_liststyle = 3
let g:netrw_browse_split = 2
let g:netrw_altv = 1
let g:netrw_winsize = 25

" coc.nvim recommended settings
set hidden
set nobackup
set nowritebackup
set cmdheight=2
set updatetime=300
set shortmess+=c

if has("patch-8.1.1564")
  set signcolumn=number
else
  set signcolumn=yes
endif

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                                 NeoVim Key remaps                          "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
" netrw
nnoremap <leader>ft :Lex<CR>

" Window
nnoremap <S-h> :vertical resize -5<cr>
nnoremap <S-l> :vertical resize +5<cr>

" Buffer
nnoremap <C-Left> :bprev<cr>
nnoremap <C-Right> :bnext<cr>
nnoremap <leader>bq :bd<cr>
nnoremap <leader>bad :%bd\|e#<cr>
"Terminal
nnoremap <leader>ot :below :10sp +terminal<cr>i<cr>
tnoremap <Esc> <C-\><C-n>

""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""
"                           Plugins configuration                            "
""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""""

"""""""""
"  CoC  "
"""""""""

let g:coc_global_extensions = [
  \ 'coc-json',
  \ 'coc-yaml',
  \ 'coc-prettier',
  \ 'coc-tsserver',
  \ 'coc-eslint',
  \ 'coc-ultisnips',
  \ ]

" Keymaps
inoremap <silent><expr> <TAB>
      \ pumvisible() ? "\<C-n>" :
      \ <SID>check_back_space() ? "\<TAB>" :
      \ coc#refresh()
inoremap <expr><S-TAB> pumvisible() ? "\<C-p>" : "\<C-h>"

function! s:check_back_space() abort
  let col = col('.') - 1
  return !col || getline('.')[col - 1] =~# '\s'
endfunction

if has('nvim')
  inoremap <silent><expr> <c-space> coc#refresh()
else
  inoremap <silent><expr> <c-@> coc#refresh()
endif

inoremap <silent><expr> <cr> pumvisible() ? coc#_select_confirm()
      \: "\<C-g>u\<CR>\<c-r>=coc#on_enter()\<CR>"

nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)

nnoremap <silent> K :call <SID>show_documentation()<CR>

function! s:show_documentation()
  if (index(['vim','help'], &filetype) >= 0)
    execute 'h '.expand('<cword>')
  elseif (coc#rpc#ready())
    call CocActionAsync('doHover')
  else
    execute '!' . &kekywordprg . " " . expand('<cword>')
  endif
endfunction

autocmd CursorHold * silent call CocActionAsync('highlight')

nmap <leader>rn <Plug>(coc-rename)

xmap <leader>f <Plug>(coc-format-selected)
nmap <leader>f <Plug>(coc-format-selected)

augroup mygroup
  autocmd!
  autocmd FileType typescript,json setl formatexpr=CocAction('formatSelected')
  autocmd User CocJumpPlaceholder call CocActionAsync('showSignatureHelp')
augroup end

nmap <leader>ac <Plug>(coc-codeaction)
xmap <leader>acs <Plug>(coc-codeaction-selected)
nmap <leader>acs <Plug>(coc-codeaction-selected)
nmap <leader>qf <Plug>(coc-fix-current)

xmap if <Plug>(coc-funcobj-i)
omap if <Plug>(coc-funcobj-i)
xmap af <Plug>(coc-funcobj-a)
omap af <Plug>(coc-funcobj-a)
xmap ic <Plug>(coc-classobj-i)
omap ic <Plug>(coc-classobj-i)
xmap ac <Plug>(coc-classobj-a)
omap ac <Plug>(coc-classobj-a)

if has('nvim-0.4.0') || has('patch-8.2.0750')
  nnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  nnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
  inoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(1)\<cr>" : "\<Right>"
  inoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? "\<c-r>=coc#float#scroll(0)\<cr>" : "\<Left>"
  vnoremap <silent><nowait><expr> <C-f> coc#float#has_scroll() ? coc#float#scroll(1) : "\<C-f>"
  vnoremap <silent><nowait><expr> <C-b> coc#float#has_scroll() ? coc#float#scroll(0) : "\<C-b>"
endif

nmap <silent> <C-s> <Plug>(coc-range-select)
xmap <silent> <C-s> <Plug>(coc-range-select)

command! -nargs=0 Format :call CocAction('format')

command! -nargs=? Fold :call     CocAction('fold', <f-args>)

command! -nargs=0 OR   :call     CocAction('runCommand', 'editor.action.organizeImport')

set statusline^=%{coc#status()}%{get(b:,'coc_current_function','')}

""""""""""""""""""""
"  MatchTagAlways  "
""""""""""""""""""""

let g:mta_use_matchparen_group = 1
let g:mta_filetypes = {
  \ 'html': 1,
  \ 'xhtml': 1,
  \ 'xml': 1,
  \ 'jinja': 1,
  \ 'javascript': 1
  \}


""""""""""""""""""""
"  vim-jsx-pretty  "
""""""""""""""""""""
let g:vim_jsx_pretty_colorful_config = 1


""""""""""""""
"  vim-test  "
""""""""""""""
let test#strategy = "neovim"
" Keymaps
nmap <silent> <leader>tn :TestNearest<CR>
nmap <silent> <leader>tf :TestFile<CR>
nmap <silent> <leader>tl :TestLast<CR>
nmap <silent> <leader>tv :TestVisit<CR>

"""""""""""
"  CtrlP  "
"""""""""""
let g:ctrlp_custom_ignore = 'node_modules\|DS_Store\|git'

"""""""""""""""""
"  vim-airline  "
"""""""""""""""""
let g:airline_powerline_fonts = 1
let g:airline#extensions#tabline#enabled = 1
let g:airline#extensions#tabline#buffer_nr_show = 1

""""""""""""""""
"  git-gutter  "
""""""""""""""""
function! GitStatus()
  let [a,m,r] = GitGutterGetHunkSummary()
  return printf('+%d ~%d -%d', a, m, r)
endfunction
set statusline+=%{GitStatus()}

""""""""""""""""""
"  vim-fugutive  "
""""""""""""""""""
" Keymaps
nnoremap <leader>gl :Gclog<cr>
nnoremap <leader>gb :Gblame<cr>
nnoremap <leader>gd :Gdiff<cr>

nnoremap <leader>el :lopen<CR>

"""""""""""""""
"  UltiSnips  "
"""""""""""""""
let g:UltiSnipsEditSplit="vertical"
let g:UltiSnipsDirectories = [
      \ stdpath('config') . '/UltiSnips']
" Keymaps
let g:UltiSnipsExpandTrigger = "<C-k>"

"""""""""""""""
"  which-key  "
"""""""""""""""
" Keymaps
nnoremap <silent> <leader> :WhichKey '<Space>'<cr>
