#!/usr/bin/env bash

status=$(playerctl -p spotify status 2> /dev/null)

getmeta()
{
  echo "$(playerctl -p spotify metadata artist) - $(playerctl -p spotify metadata title)"
  return 0
}

if [ "$status" = "Playing" ]; then
  echo "$(getmeta)" 
elif [ "$status" = "Paused" ]; then
  echo "P | $(getmeta)"
else
  echo ""
fi

