#!/bin/sh

Load() {
	SYSLOAD=$(systat -b | awk 'NR==3 { print $4"  "$5"  "$6 }')
	echo -n "%{F#00FF00}%{B#000000}Load:%{F-}%{B-} $SYSLOAD "
}
