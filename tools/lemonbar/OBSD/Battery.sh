#!/bin/sh

Battery() {
	ADAPTER=$(apm -a)
	if [ $ADAPTER = 0 ] ; then
		echo -n "%{F#FF0000}%{B#000000}AC: %{F-}%{B-}"
	elif [ $ADAPTER = 1 ] ; then
		echo -n "%{F#00FF00}%{B#000000}AC: %{F-}%{B-}"
	else
		echo -n "%{F#0000FF}%{B#000000}AC: %{F-}%{B-}"
	fi
	BATTERY=$(apm -l)
	if [ $BATTERY -gt 66 ] ; then
		echo -n "%{F#00FF00}%{B#000000}$BATTERY%% %{F-}%{B-}"
	elif [ $BATTERY -gt 33 ] ; then
		echo -n "%{F#FFFF00}%{B#000000}$BATTERY%% %{F-}%{B-}"
	else
		echo -n "%{F#FF0000}%{B#000000}$BATTERY%% %{F-}%{B-}"
	fi
	BATTERY=$(apm -m)
	[[ "$BATTERY" != "unknown" ]] && echo -n " ($BATTERY m) "
}
