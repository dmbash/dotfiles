#!/bin/sh

PRG="$0"

# need this for relative symlinks
while [ -h "$PRG" ] ; do
   PRG=`readlink "$PRG"`
done

scriptdir=`dirname "$PRG"`

. $scriptdir/Clock.sh
. $scriptdir/Battery.sh
. $scriptdir/Cpu.sh
. $scriptdir/Load.sh
. $scriptdir/Volume.sh

while true; do
    echo "%{r}[$(Cpu)][$(Load)][$(Volume)][$(Battery)][$(Clock)]"
    sleep 1;
done     


