#!/bin/sh

Cpu() {
    set -A cpu_names $(iostat -C | sed -n '2,2p')
    set -A cpu_values $(iostat -C | sed -n '3,3p')
    CPULOAD=$((100-${cpu_values[5]}))
    CPUTEMP=$(sysctl hw.sensors.cpu0.temp0 | awk -F "=" '{ gsub("deg", "", $2); print $2 }')
    CPUSPEED=$(apm | sed '1,2d;s/.*(//;s/)//')
    if [ $CPULOAD -ge 90 ] ; then
	echo -n "%{F#FF0000}%{B#000000}"
    elif [ $CPULOAD -ge 80 ] ; then
	echo -n "%{F%FFFF00}%{B#000000}"
    else
	echo -n "%{F#00FF00}%{B#000000}"
    fi
    echo -n "CPU: $CPULOAD%% %{F-}%{B-}$CPUTEMP $CPUSPEED "
}
