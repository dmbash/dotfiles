#!/bin/sh

Volume() {
    MUTE=$(sndioctl output.mute | awk -F '=' '{ print $2 }')
    LEVEL=$(sndioctl output.level | awk -F '(=|,)' '{ print int($2 * 100) }')
    
    if [ "$MUTE" = "1" ] ; then
		echo -n "%{F#FF0000}%{B#000000}"
	else
		echo -n "%{F#00FF00}%{B#000000}"
	fi
	echo -n "Vol:%{F-}%{B-} $LEVEL%%"
}
