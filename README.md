## To enable RPMFusion in Fedora

```
sudo dnf install https://download1.rpmfusion.org/free/fedora/rpmfusion-free-release-$(rpm -E %fedora).noarch.rpm
sudo dnf install https://download1.rpmfusion.org/nonfree/fedora/rpmfusion-nonfree-release-$(rpm -E %fedora).noarch.rpm
```

## Shell

#### Bash
* [oh-my-bash](https://github.com/ohmybash/oh-my-bash)

#### Zsh
* [oh-my-zsh](https://github.com/ohmyzsh/ohmyzsh)
* [dracula theme](https://github.com/dracula/zsh)

## Terminal emulator

* [alacritty](https://github.com/alacritty/alacritty)

To install alacritty in Fedora type with sudo:
```
dnf copr enable pschyska/alacritty
dnf install alacritty
```

## File manager

* [nnn](https://github.com/jarun/nnn)

## Window Manager

Font - [FiraCode](https://github.com/tonsky/FiraCode)

* [i3-gaps](https://github.com/Airblader/i3)
* [xmonad](https://xmonad.org/)
* [polybar](https://github.com/polybar/polybar) - status bar

To install i3-gaps in Fedora type with sudo:
```
dnf copr enable fuhrmann/i3-gaps
dnf install i3-gaps
```

To change wallpaper simply add symlink to ~/.wallpaper

## Editor

I'm using Neovim with vim-plug as plugin manager

* [neovim](https://neovim.io/)
* [config](./nvim/init.vim)

## Web browser

* [firefox](https://firefox.com)

## Packages

* [aerc](https://aerc-mail.org) - TUI Email client
* [playerctl](https://github.com/acrisci/playerctl) - Tool for controlling media players
* [rofi](https://github.com/davatorium/rofi) - dmenu replacement
* [feh](http://feh.finalrewind.org) - Image viewer
* [dunst](https://github.com/dunst-project/dunst) - Notification daemon
* [flameshot](https://github.com/lupoDharkael/flameshot) - Screenshot tool
* [udiskie](https://github.com/coldfix/udiskie) - Tool for managing removeable media
* [zathura](http://pwmt.org/projects/zathura) - Document viewer
* [zathura-pdf-mupdf](http://pwmt.org/projects/zathura/plugins/zathura-pdf-mupdf) - PDF plugin for zathura
* [Stretchly](https://hovancik.net/stretchly) - Break timer
* [legendary](https://github.com/derrod/legendary) - OSS Epic Games Launcher
* [light](https://haikarainen.github.io/light) - Backlight control
