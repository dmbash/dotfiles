import XMonad

import Data.List(sortBy)
import Data.Function(on)

import Control.Monad(forM_, join)

import XMonad.Prompt.ConfirmPrompt -- For comfirming exit from xmonad
import System.Exit

import XMonad.Layout.Renamed(renamed, Rename(Replace))
import XMonad.Layout.Spacing
import XMonad.Layout.ResizableTile
import XMonad.Layout.NoBorders

import XMonad.Actions.SpawnOn

import XMonad.Hooks.EwmhDesktops
import XMonad.Hooks.ManageHelpers
import XMonad.Hooks.ManageDocks
import XMonad.Hooks.Place
import XMonad.Hooks.SetWMName;

import XMonad.Util.NamedWindows(getName)
import XMonad.Util.NamedActions
import XMonad.Util.EZConfig
import XMonad.Util.SpawnOnce
import XMonad.Util.Run(safeSpawn)

import qualified XMonad.StackSet as W

main = do
  forM_ [".xmonad-workspace-log", ".xmonad-title-log", ".xmonad-layout-log"] $ \file -> do
    safeSpawn "mkfifo" ["/tmp/" ++ file]

  xmonad
    $ ewmh
    $ docks
    $ addDescrKeys ((mod1Mask, xK_F1), xMessage) myAdditionalKeys
    $ myConfig

------------------------------------------------------------------------
--                              Globals                               --
------------------------------------------------------------------------

myTerminal = "alacritty"
myBrowser = "brave-browser"
myFileManager = myTerminal ++ " -e nnn -c"
myModMask = mod1Mask
------------------------------------------------------------------------
--                            Colorscheme                             --
------------------------------------------------------------------------


------------------------------------------------------------------------
--                               Layout                               --
------------------------------------------------------------------------
myLayouts = avoidStruts $ smartBorders (tiled ||| full)
  where
    tiled = renamed [Replace "Tall"]
           $ spacingRaw True (Border 10 0 10 0) True (Border 0 10 0 10) True
           $ ResizableTall 1 (3/100) (1/2) []

    full = renamed [Replace "Full"]
           $ noBorders(Full)

    nmaster = 1

    ratio = 1/2

    delta = 3/100

------------------------------------------------------------------------
--                             Workspaces                             --
------------------------------------------------------------------------
myWorkspaces =
  ["1: \61704"
  , "2: \61612"
  , "3: \61729"
  , "4: \61723"
  , "5: \63612"
  , "6: \61946"
  , "7: \61912"
  , "8: \61890"
  , "9"]

------------------------------------------------------------------------
--                            Keybindings                             --
------------------------------------------------------------------------

myAdditionalKeys c = (subtitle "Custom Keys":) $ mkNamedKeymap c $
  myProgramKeys ++ myWmKeys ++ myMediaKeys

myProgramKeys =
  [("<Print>",                addName "Make a screenshot"   $ spawn "flameshot gui")
  ,("M-b",                    addName "Open Firefox"        $ spawn myBrowser)
  ,("M-C-r",                  addName "Open File manager"   $ spawn myFileManager)
  ,("M-d",                    addName "Open Rofi"           $ spawn "rofi -show combi")
  ,("M-C-S-m",                addName "Only monitor"        $ spawn "$HOME/.screenlayout/only_monitor.sh")
  ,("M-C-S-b",                addName "Both displays"       $ spawn "$HOME/.screenlayout/main.sh")
  ,("M-<Return>",             addName "Open terminal"       $ spawn myTerminal)
  ]

myWmKeys =
  [("M-S-q",                  addName "Close window"        $ kill)
  ,("M-S-e",                  addName "Exit xmonad"         $ confirmPrompt def "Exit" $ io (exitWith ExitSuccess))
  ]

myMediaKeys =
  [("<XF86AudioRaiseVolume>", addName "Raise volume"        $ spawn "pactl set-sink-volume @DEFAULT_SINK@ +10%")
  ,("<XF86AudioLowerVolume>", addName "Lower volume"        $ spawn "pactl set-sink-volume @DEFAULT_SINK@ -10%")
  ,("<XF86AudioMute>",        addName "Toggle volume mute"  $ spawn "pactl set-sink-mute @DEFAULT_SINK@ toggle")
  ,("<XF86AudioPrev>",        addName "Previous track"      $ spawn "playerctl previous")
  ,("<XF86AudioNext>",        addName "Next track"          $ spawn "playerctl next")
  ,("<XF86AudioPlay>",        addName "Play-pause track"    $ spawn "playerctl play-pause")
  ,("<XF86MonBrightnessUp>",    addName "Increase brightness" $ spawn "light -A 10")
  ,("<XF86MonBrightnessDown>",  addName "Decrease brightness" $ spawn "light -U 10")
  ]


------------------------------------------------------------------------
--                            Manage hook                             --
------------------------------------------------------------------------
myManageHook = composeAll
  [resource   =? "desktop_window"               --> doIgnore
  ,className  =? "TelegramDesktop"              --> doShift( myWorkspaces !! 6 )
  ,role       =? "pop-up"                       --> doFloat
  ,className  =? "Steam"                        --> doShift( myWorkspaces !! 3 )
  ]
  where
    role = stringProperty "WM_WINDOW_ROLE"

myManageHook' = composeOne [ isFullscreen -?> doFullFloat ]


------------------------------------------------------------------------
--                              Log hook                              --
------------------------------------------------------------------------
myLogHook = eventLogHook

eventLogHook = do
  winset <- gets windowset
  title <- maybe (return "") (fmap show . getName) . W.peek $ winset
  let currWs = W.currentTag winset
  let wss = map W.tag $ W.workspaces winset
  let wsStr = join $ map (fmt currWs) $ sort' wss
  let lStr = description $ (W.layout . W.workspace . W.current) winset

  io $ appendFile "/tmp/.xmonad-title-log" (take 20 title ++ "\n")
  io $ appendFile "/tmp/.xmonad-workspace-log" (wsStr ++ "\n")
  io $ appendFile "/tmp/.xmonad-layout-log" (lStr ++ "\n")

  where fmt currWs ws
          | currWs == ws = "[" ++ ws ++ "]"
          | otherwise    = " " ++ ws ++ " "
        sort' = sortBy (compare `on` (!!0))


------------------------------------------------------------------------
--                              Startup                               --
------------------------------------------------------------------------

myStartupHook = do
  spawn "$HOME/.config/polybar/launch.sh"
  spawnOnce "light-locker &"
  spawnOnce "xautolock -time 30 -locker 'light-locker-command -l'"
  spawnOnce "dunst"
  spawnOnce "udiskie"
  spawnOnce "feh --bg-scale $HOME/.wallpaper"
  spawnOnce "nm-applet"
  spawnOnce "setxkbmap -layout \"us,ru\" -option \"grp:alt_caps_toggle\""
  spawnOnce "/opt/Stretchly/stretchly"
  spawnOnOnce     ( myWorkspaces !! 5 ) (myTerminal ++ " -e aerc")
  spawnOnOnce     ( myWorkspaces !! 1 ) (myBrowser)
  spawnNOnOnce 2  ( myWorkspaces !! 2 ) (myTerminal)
  spawnOnOnce     ( myWorkspaces !! 2 ) (myTerminal ++ " -e nvim")

------------------------------------------------------------------------
--                             Event Hook                             --
------------------------------------------------------------------------
myHandleEventHook = handleEventHook def
  <+> fullscreenEventHook

------------------------------------------------------------------------
--                               Config                               --
------------------------------------------------------------------------

myConfig = def
  { terminal        = myTerminal
  , layoutHook      = myLayouts
  , handleEventHook = myHandleEventHook
  , manageHook      = placeHook(smart(0.5, 0.5))
    <+> manageSpawn
    <+> myManageHook
    <+> myManageHook'
    <+> manageDocks
    <+> manageHook def
  , startupHook     = setWMName "LG3D"
    <+> myStartupHook
  , logHook         = myLogHook
  , workspaces      = myWorkspaces
  , modMask         = myModMask
  }
