# Office

- LibreOffice

# Video players

- mpv

# Entertainment

- steam
- minigalaxy - GOG games
- quiterss - GUI RSS reader

# Communication

- telegram-desktop

# Development

- Eclipse - IDE for Java development
- maven
- git
- docker
- VSCode - for frontend development
- neovim - enriched vim (text editor)
- postgresql - DBMS
- yarn - npm alternative for JavaScript packages

# System utilities

- light - controls brightness of the screen
- zathura (with zathura-pdf-mupdf) - document viewer
- udiskie - utility for auto-mounting removable devices
- thurar - GUI File manager
- qbittorrent - Torrent files manager
- picom - desktop transparency manager
- flameshot - Screen-shot tool with GUI support
- firefox - Main browser
- dunst - nofication manager
- lsd - nicer ls command
- stalonetray - suckless system tray
- dwm - window manager
- starship - better bash prompt out of the box
